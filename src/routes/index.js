import Home from '../pages/Home/index.vue';
import SignUp from '../pages/SignUp/index.vue';
import Dashboard from '../pages/Dashboard/index.vue';
const routes = [
    {
      path:'/',
      name:'home',
      component:Home
    },
    {
      path:'/signup',
      name:'signUp',
      component:SignUp
    },
    {
      path:'/dashboard',
      name:'dashboard',
      component:Dashboard
    },

  ];

  export default routes;
